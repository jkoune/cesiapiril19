**Commands :**

mvn clean install

mvn spring-boot:run

**Subject :**

https://docs.google.com/document/d/1rZkW6QRfGHnrCQ24ctZhNC7HhEhrwWOOCrolT4mg4uw/edit

**Postman Requests :**

https://www.getpostman.com/collections/dc0b688772ed0ec804e7

-----
DB is initialized with mock data (V5 migration)