package fr.cesi.ril19.javaproject.controllers.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.cesi.ril19.javaproject.entities.Task;
import fr.cesi.ril19.javaproject.services.TasksService;

@RestController
@RequestMapping("/api/v1/projects/{idProject}/tasks")
public class TasksController {

    private TasksService tasksService;

    @Autowired
    TasksController(TasksService tasksService) {
        this.tasksService = tasksService;
    }

    @RequestMapping("")
    public List<Task> getAll(@PathVariable Long idProject) {
        return this.tasksService.getTasksFromProject(idProject);
    }
}
