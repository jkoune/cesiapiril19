package fr.cesi.ril19.javaproject.controllers.api;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.cesi.ril19.javaproject.entities.Project;
import fr.cesi.ril19.javaproject.entities.User;
import fr.cesi.ril19.javaproject.services.UsersService;
import fr.cesi.ril19.javaproject.utils.CsvUtils;
import javassist.NotFoundException;

@RestController
@RequestMapping("/api/v1/users")
public class UsersController {

    private UsersService usersService;

    @Autowired
    UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("")
    public List<User> getAll() {
        return this.usersService.getAllUsers();
    }

    @PostMapping("")
    public User addUser(@RequestBody User newUser) {
        return this.usersService.addUser(newUser);
    }

    @GetMapping("/{id}")
    public User getOne(@PathVariable Long id) throws NotFoundException {
        return this.usersService.getUser(id);
    }

    @PutMapping("/{id}")
    public User editUser(@RequestBody User newUser, @PathVariable Long id) throws NotFoundException {
        return this.usersService.editUser(newUser, id);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) throws NotFoundException {
        this.usersService.deleteUser(id);
    }

    @GetMapping("/{id}/projects")
    public List<Project> getProjectsFromUser(@PathVariable Long id) {
        return this.usersService.getProjectsFromUserId(id);
    }

    // import users via CSV file
    @PostMapping(path = "/import", consumes = "multipart/form-data")
    public List<User> importUsers(@RequestBody MultipartFile csvBody)
            throws IOException {
        return this.usersService.importAllUsers(CsvUtils.read(User.class, csvBody.getInputStream()));
    }
}
