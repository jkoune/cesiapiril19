package fr.cesi.ril19.javaproject.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "TASKS")
@Data
@NoArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String num;

    private Long hourcost;

    private Long duration;

    @NotNull
    private Long idproject;

    // can fail toString method
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idproject", insertable = false, updatable = false, nullable = false)
    @JsonIgnore
    private Project project;

    public Task(String num, Long hourCost, Long duration, Long idproject) {
        this.num = num;
        this.hourcost = hourCost;
        this.duration = duration;
        this.idproject = idproject;
    }

    public Task(Long id, String num, Long hourCost, Long duration, Long idproject) {
        this(num, hourCost, duration, idproject);
        this.id = id;
    }
}