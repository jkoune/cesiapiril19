package fr.cesi.ril19.javaproject.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.cesi.ril19.javaproject.entities.Task;

@Repository
public interface TasksRepository extends CrudRepository<Task, Long> {

    @Query(value = "SELECT * FROM TASKS t WHERE t.idproject=:idProject", nativeQuery = true)
    Iterable<Task> findByIdProject(Long idProject);
    
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM TASKS t WHERE t.idproject=:idProject", nativeQuery = true)
    void cleanTasksByIdProject(Long idProject);
}