package fr.cesi.ril19.javaproject.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.cesi.ril19.javaproject.entities.User;

@Repository
public interface UsersRepository extends CrudRepository<User, Long> {

}
