package fr.cesi.ril19.javaproject.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.cesi.ril19.javaproject.entities.Task;
import fr.cesi.ril19.javaproject.repositories.ProjectsRepository;
import fr.cesi.ril19.javaproject.repositories.TasksRepository;
import fr.cesi.ril19.javaproject.utils.ProjectNotFoundException;

@Service
public class TasksService {

    TasksRepository tasksRepo;
    ProjectsRepository projectsRepo;

    @Autowired
    TasksService(TasksRepository tasksRepo, ProjectsRepository projectsRepo) {
        this.tasksRepo = tasksRepo;
        this.projectsRepo = projectsRepo;
    }

    public List<Task> getTasksFromProject(Long idProject) {
        if (!this.projectsRepo.existsById(idProject)) {
            throw new ProjectNotFoundException(idProject);
        }
        return (List<Task>) this.tasksRepo.findByIdProject(idProject);
    }
}
