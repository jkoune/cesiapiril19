CREATE TABLE USERS (
    id int IDENTITY NULL,
    lastname VARCHAR(255) NOT NULL,
    firstname VARCHAR(255) NOT NULL,
    email VARCHAR(255),
    tel VARCHAR(10)
)