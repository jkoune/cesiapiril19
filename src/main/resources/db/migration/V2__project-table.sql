CREATE TABLE PROJECTS (
    id int IDENTITY NOT NULL,
    iduser int NOT NULL,
    title VARCHAR(20) NOT NULL,
    description VARCHAR(255),
    mode VARCHAR(7),
    budget int,
    startDate DATE,
    workDays int,
    foreign key (iduser) REFERENCES USERS(id)
)