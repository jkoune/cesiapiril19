CREATE TABLE TASKS (
    id int IDENTITY NOT NULL,
    num VARCHAR(30) NOT NULL,
    hourCost int,
    duration int,
    idProject int,
    foreign key (idProject) REFERENCES PROJECTS(id)
)